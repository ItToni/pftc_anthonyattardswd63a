﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.DataAccess;
using WebApplication1.Models;
using Google.Cloud.Storage.V1;
using Google.Apis.Storage.v1.Data;

namespace WebApplication1.Controllers
{
    public class FilesController : Controller
    {
        [Authorize]
        [HttpGet]
        public ActionResult Create()
        { return View(); }

        [Authorize]
        [HttpPost]
        public ActionResult Create(File f, HttpPostedFileBase file)
        {
            FilesRepository fr = new FilesRepository();
            //upload image related to product on the bucket
            try
            {
                #region Uploading file on Cloud Storage
                var storage = StorageClient.Create();
                string link = "";
                using (var fi = file.InputStream)
                {
                    var filename = Guid.NewGuid() + System.IO.Path.GetExtension(file.FileName);
                    var storageObject = storage.UploadObject("pftc_bucket", filename, null, fi);
                    link = "https://storage.cloud.google.com/pftc_bucket/" + filename;

                    if (null == storageObject.Acl)
                    {
                        storageObject.Acl = new List<ObjectAccessControl>();
                    }

                    storageObject.Acl.Add(new ObjectAccessControl()
                    {
                        Bucket = "pftc_bucket",
                        Entity = $"user-" + User.Identity.Name,
                        Role = "OWNER", //OWNER
                    });

                    storageObject.Acl.Add(new ObjectAccessControl()
                    {
                        Bucket = "pftc_bucket",
                        Entity = $"user-" + f.To, 
                        Role = "READER", //READER
                    });

                    var updatedObject = storage.UpdateObject(storageObject, new UpdateObjectOptions()
                    {
                        // Avoid race conditions.
                        IfMetagenerationMatch = storageObject.Metageneration,
                    });

                    //store details in a relational db including the filename/link
                    #endregion

                    fr.MyConnection.Open();
                    fr.MyTransaction = fr.MyConnection.BeginTransaction();

                    #region Storing details of file in db [INCOMPLETE]
                    f.OwnerFk = User.Identity.Name;
                    f.Link = link;
                    

                    fr.AddFile(f);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = "File failed to be created; " + ex.Message;
                fr.MyTransaction.Rollback();
                new LogsRepository().LogError(ex);
            }

            fr.MyTransaction.Commit();
            fr.MyConnection.Close();

            #region Updating Cache with latest list of Files from db

            CacheRepository cr = new CacheRepository();
            cr.UpdateCache(fr.GetFiles(User.Identity.Name));

            #endregion
            PubSubRepository psr = new PubSubRepository();
            psr.AddToEmailQueue(f); //adding it to queue to be sent as an email later on.
            ViewBag.Message = "File created successfully";

            return RedirectToAction("Index");
        }


        // GET: Files
        [Authorize]
        public ActionResult Index()
        {
            #region get files of db - removed....instead insert next region
            //FilesRepository fr = new FilesRepository();
            //var file = fr.GetFiles(User.Identity.Name); //gets files from db
            #endregion

            #region instead of getting files from DB, to make your application faster , you load them from the cache
            CacheRepository cr = new CacheRepository();
            FilesRepository fr = new FilesRepository();

            cr.UpdateCache(fr.GetFiles(User.Identity.Name));
            var files = cr.GetFilesFromCache();
            #endregion

            return View("Index",files);
        }

        // GET: Files
        [Authorize]
        public ActionResult SharedFilesWithUser()
        {
            #region get files of db - removed....instead insert next region
            //FilesRepository fr = new FilesRepository();
            //var file = fr.GetFiles(User.Identity.Name); //gets files from db
            #endregion

            #region instead of getting files from DB, to make your application faster , you load them from the cache
            CacheRepository cr = new CacheRepository();
            FilesRepository fr = new FilesRepository();

            cr.UpdateCache(fr.GetSharedFilesWithUser(User.Identity.Name));
            var files = cr.GetFilesFromCache();
            #endregion
            new LogsRepository().WriteLogEntry(User.Identity.Name + " accessed shared files!");
            return View("SharedFilesWithUser", files);
        }

        public ActionResult Delete(int id)
        {
            FilesRepository fr = new FilesRepository();
            fr.DeleteFile(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteAll(int[] ids)
        {
            //1. Requirement when opening a transaction: Connection has to be opened

            FilesRepository fr = new FilesRepository();
            fr.MyConnection.Open();

            fr.MyTransaction = fr.MyConnection.BeginTransaction(); //from this point onwards all code executed against the db will remain pending

            try
            {
                foreach (int id in ids)
                {
                    fr.DeleteFile(id);
                }

                fr.MyTransaction.Commit(); //Commit: you are confirming the changes in the db
            }
            catch (Exception ex)
            {
                //Log the exception on the cloud
                fr.MyTransaction.Rollback(); //Rollback: it will reverse all the changes done within the try-clause in the db
                new LogsRepository().LogError(ex);
            }

            fr.MyConnection.Close();

            return RedirectToAction("Index");
        }
    }
}