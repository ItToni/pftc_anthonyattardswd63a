﻿using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.Json;
using System.Web;
using WebApplication1.Controllers;

namespace WebApplication1.DataAccess
{
    public class PubSubRepository
    {
        TopicName tn;
        SubscriptionName sn;
        public PubSubRepository()
        {
            tn = new TopicName("ProgForTheCloudAA", "pftc_topic");  //A Queue/Topic will be created to hold the emails to be sent.  It will always have the same name DemoTopic, which you can change
            sn = new SubscriptionName("ProgForTheCloudAA", "pftc_subscription");  //A Subscription will be created to hold which messages were read or not.  It will always have the same name DemoSubscription, which you can change
        }
        private Topic CreateGetTopic()
        {
            PublisherServiceApiClient client = PublisherServiceApiClient.Create();   //We check if Topic exists, if no we create it and return it
            TopicName tn = new TopicName("ProgForTheCloudAA", "pftc_topic");
            try
            {
                return client.GetTopic(tn);
            }
            catch(Exception ex)
            {
                new LogsRepository().LogError(ex);
                return client.CreateTopic(tn);
            }
        }

        /// <summary>
        /// Publish method: uploads a message to the queue
        /// </summary>
        /// <param name="f"></param>
        public void AddToEmailQueue(Models.File f)
        {
            PublisherServiceApiClient client = PublisherServiceApiClient.Create();
            var t = CreateGetTopic();


            string serialized = JsonSerializer.Serialize(f, typeof(Models.File));
            serialized = KeyRepository.Encrypt(serialized);
            List<PubsubMessage> messagesToAddToQueue = new List<PubsubMessage>(); // the method takes a list, so you can upload more than 1 message/item/product at a time
            PubsubMessage msg = new PubsubMessage();
            msg.Data = ByteString.CopyFromUtf8(serialized);
            
            messagesToAddToQueue.Add(msg);


            client.Publish(t.TopicName, messagesToAddToQueue); //committing to queue

        }


        private Subscription CreateGetSubscription()
        {
            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();  //We check if Subscription exists, if no we create it and return it

            try
            {
                return client.GetSubscription(sn);
            }
            catch(Exception ex)
            {
                new LogsRepository().LogError(ex);
                return client.CreateSubscription(sn, tn, null, 30);
            }

        }

        public string DownloadEmailFromQueueAndSend()
        {
            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();

            var s = CreateGetSubscription(); //This must be called before being able to read messages from Topic/Queue
            var pullResponse = client.Pull(s.SubscriptionName, true, 1); //Reading the message on top; You can read more than just 1 at a time
            if (pullResponse != null)
            {
                if (pullResponse.ReceivedMessages.Count > 0)
                {
                    string toDeserialize = pullResponse.ReceivedMessages[0].Message.Data.ToStringUtf8(); //extracting the first message since in the previous line it was specified to read one at a time. if you decide to read more then a loop is needed
                    toDeserialize = KeyRepository.Decrypt(toDeserialize);
                    Models.File deserialized = JsonSerializer.Deserialize<Models.File>(toDeserialize); //Deserializing since when we published it we serialized it

                    //Send Email with deserialized. Documentation: https://docs.microsoft.com/en-us/dotnet/api/system.net.mail.smtpclient?view=netframework-4.8

                    MailMessage message = new MailMessage();
                    SmtpClient smtp = new SmtpClient();
                    message.From = new MailAddress(deserialized.OwnerFk);
                    message.To.Add(new MailAddress(deserialized.To));
                    message.Subject = "File Shared!";
                    message.IsBodyHtml = true; //to make message body as html  
                    message.Body = "A file has been shared with you. Link: " + deserialized.Link;
                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com"; //for gmail host  
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential("pftcanthony@gmail.com", "progforcloud2020$");

                    //go on google while you are logged in in this account > search for lesssecureapps > turn it to on

                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(message);


                    List<string> acksIds = new List<string>();
                    acksIds.Add(pullResponse.ReceivedMessages[0].AckId); //after the email is sent successfully you acknolwedge the message so it is confirmed that it was processed

                    client.Acknowledge(s.SubscriptionName, acksIds.AsEnumerable());

                    new LogsRepository().WriteLogEntry(deserialized.OwnerFk + " shared a file successfully!");
                    return "Email Sent";
                }
                else
                {
                    new LogsRepository().WriteLogEntry("Email was not Sent");
                    return "Email not Sent";
                }
            }
            return "Email not Sent";
        }
    }
}