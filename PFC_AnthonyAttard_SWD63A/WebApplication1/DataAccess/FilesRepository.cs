﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.DataAccess
{
    public class FilesRepository: ConnectionClass
    {
        public FilesRepository() : base() { }

        //public List<File> GetFiles()
        //{
        //    string sql = "Select Id, Name, Ownerfk from files";

        //    NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            

        //    MyConnection.Open();
        //    List<File> results = new List<File>();

        //    using (var reader = cmd.ExecuteReader())
        //    {
        //        while(reader.Read())
        //        {
        //            File f = new File();
        //            f.Id = reader.GetInt32(0);
        //            f.Name = reader.GetString(1);
        //            f.OwnerFk = reader.GetString(2);
        //            results.Add(f);
        //        }
        //    }

        //    MyConnection.Close();

        //    return results;
        //}

        public List<File> GetFiles(string email)
        {
            string sql = "Select Id, Name, Ownerfk from files where ownerfk=@email";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);

            List<File> results = new List<File>();

            MyConnection.Open();

            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    File f = new File();
                    f.Id = reader.GetInt32(0);
                    f.Name = reader.GetString(1);
                    f.OwnerFk = reader.GetString(2);
                    results.Add(f);
                }
            }

            MyConnection.Close();

            return results;
        }

        public List<File> GetSharedFilesWithUser(string email)
        {
            List<File> results = new List<File>();
            try
            {
                string sql = "Select Id, Name, Ownerfk, Link from files where sharedwith=@email";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
                cmd.Parameters.AddWithValue("@email", email);

                MyConnection.Open();
               
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        File f = new File();
                        f.Id = reader.GetInt32(0);
                        f.Name = reader.GetString(1);
                        f.OwnerFk = reader.GetString(2);
                        f.Link = reader.GetString(3);
                        results.Add(f);
                    }
                }

                MyConnection.Close();

            }
            catch(Exception ex)
            {
                new LogsRepository().LogError(ex);
            }

            return results;
        }

        public void AddFile(File f)
        {
            string sql = "INSERT into files (name, ownerfk, sharedwith, link) values (@name, @ownerfk, @sharedwith, @link)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@name", f.Name);
            cmd.Parameters.AddWithValue("@ownerfk", f.OwnerFk);
            cmd.Parameters.AddWithValue("@sharedwith", f.To);
            cmd.Parameters.AddWithValue("@link", f.Link);
            cmd.ExecuteNonQuery();

            new LogsRepository().WriteLogEntry(f.OwnerFk + " uploaded a file successfully!");
        }

        public void DeleteFile(int id)
        {
            string sql = "Delete from files where Id = @id";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);

            bool connectionOpenedInThisMethod = false;

            if (MyConnection.State == System.Data.ConnectionState.Closed)
            {
                MyConnection.Open();
                connectionOpenedInThisMethod = true;
            }

            if(MyTransaction != null)
            {
                cmd.Transaction = MyTransaction; //to participate in the opened trasaction (somewhere else), assign the Transaction property to the opened transaction
            }
            cmd.ExecuteNonQuery();

            if (connectionOpenedInThisMethod == true)
            {
                MyConnection.Close();
            }
        }
    }
}